#!/usr/bin/python3


class Car:
    def __init__(self, car_vin: str, car_engine: str):
        self.car_vin = car_vin
        self.car_engine = car_engine
        print("New car was made"
              .format(self.car_vin, self.car_engine))
        self.wheel_number = 4

    def rides(self) -> None:
        print("Vrum, vrum, vrum!")

    def __del__(self):
        print("Car was trashed")
        print("-" * 20)
        self.car_vin = ""
        self.car_engine = ""


# Class Audi inherit after Car -> variable inherit
class Audi(Car):
    pass


a4 = Audi("1234", "2.4")
print("VIN = ", a4.car_vin, "| Capacity = ", a4.car_engine,
      "| Has ", a4.wheel_number, " wheels.")
del a4
print("-" * 20)


# Class Skoda inherit after Car -> method inherit
class Skoda(Car):
    pass


fabia = Skoda("2345", "1.9")
fabia.rides()
del fabia
print("-" * 20)


# Method inherit and super() function
class VW(Car):
    def rides(self) -> None:
        print("Starting engine.")
        super().rides()


cr1 = Car("3456", "4.2")
cr1.rides()
del cr1

passat = VW("4567", "1.9")
passat.rides()
