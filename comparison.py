#!/usr/bin/python3


# Same name function
def is_same():
    name: str = input("What is your name: ")
    if name == "rafal":
        print("Hello again Rafal. I was waiting for you.")
    else:
        print("Welcome ", name, "for the first time!")


is_same()
print("-" * 20)


# Compare triangles
class Triangle:
    def __init__(self, base: int, height: int):
        self.base: int = base
        self.height: int = height

    def __eq__(self, other):
        return self.base == other.base and self.height == other.height

    def __lt__(self, other):
        return self.base < other.base and self.height < other.height

    def __gt__(self, other):
        return self.base > other.base and self.height > other.height

    def __le__(self, other):
        return self.base <= other.base and self.height <= other.height

    def __ge__(self, other):
        return self.base >= other.base and self.height >= other.height

    def __ne__(self, other):
        return self.base != other.base and self.height != other.height


t1 = Triangle(2, 3)
t2 = Triangle(1, 2)

print("t1 = t2:", t1 == t2)
print("t1 < t2:", t1 < t2)
print("t1 > t2:", t1 > t2)
print("t1 <= t2:", t1 <= t2)
print("t1 >= t2:", t1 >= t2)
print("t1 != t2:", t1 != t2)

print("-" * 20)

# Compare cubes


class Cylinder:
    def __init__(self, radius: int, height: int):
        self.radius: int = radius
        self.height: int = height
        self.volume = 3.14 * radius * radius

    def __eq__(self, other):
        return self.volume == other.volume

    def __lt__(self, other):
        return self.volume < other.volume

    def __gt__(self, other):
        return self.volume > other.volume

    def __le__(self, other):
        return self.volume <= other.volume

    def __ge__(self, other):
        return self.volume >= other.volume

    def __ne__(self, other):
        return self.volume != other.volume


c1 = Cylinder(10, 5)
print("c1 = ", c1.volume)
c2 = Cylinder(5, 10)
print("c2 = ", c2.volume)

print("c1 = c2:", c1 == c2)
print("c1 < c2:", c1 < c2)
print("c1 > c2:", c1 > c2)
print("c1 <= c2:", c1 <= c2)
print("c1 >= c2:", c1 >= c2)
print("c1 != c2:", c1 != c2)
