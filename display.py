#!/usr/bin/python3


# Input and print first name, last name and age
first_name: str = input("What is your first name: ")
last_name: str = input("What is your last name: ")
age: str = input("How old are you: ")

print("You are", first_name, last_name, "and you are ", age)
print("-" * 20)


# Function converting input result from str to int
def number() -> None:
    int_number: str = input("Give integer: ")
    print(int(int_number))


number()
print("-" * 20)


# Class describing flower
class Rose:
    def __init__(self, colour: str, smell: str, height: str):
        self.colour = colour
        self.smell = smell
        self.height = height

    def __str__(self):
        return "Rose is {}. It smells {} and is {} cm".format(self.colour, self.smell, self.height)


col = input("What is the colour of this rose: ")
sm = input("What does it smell like: ")
hig = input("How high is it in cm: ")

small_rose = Rose(col, sm, hig)

print(small_rose)
