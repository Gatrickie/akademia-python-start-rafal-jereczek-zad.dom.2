#!/usr/bin/python3

class Vehicle:

    def __init__(self, kind: str):
        self.kind: str = kind

    def __eq__(self, other):
        return self.kind == other.kind

    def __lt__(self, other):
        return self.kind < other.kind

    def __gt__(self, other):
        return self.kind > other.kind

    def __le__(self, other):
        return self.kind <= other.kind

    def __ge__(self, other):
        return self.kind >= other.kind

    def __ne__(self, other):
        return self.kind != other.kind


class Wheeled(Vehicle):

    def __init__(self, kind: str, name: str):
        super().__init__(kind)
        self.name: str = name

    def __eq__(self, other):
        return super().__eq__(other) \
               and self.name == other.name

    def __lt__(self, other):
        return super().__lt__(other) \
               and self.name < other.name

    def __gt__(self, other):
        return super().__gt__(other) \
               and self.name > other.name

    def __le__(self, other):
        return super().__le__(other) \
               and self.name <= other.name

    def __ge__(self, other):
        return super().__ge__(other) \
               and self.name >= other.name

    def __ne__(self, other):
        return super().__ne__(other) \
               and self.name != other.name


class Air(Vehicle):

    def __init__(self, kind: str, name: str):
        super().__init__(kind)
        self.name: str = name

    def __eq__(self, other):
        return super().__eq__(other) \
               and self.name == other.name

    def __lt__(self, other):
        return super().__lt__(other) \
               and self.name < other.name

    def __gt__(self, other):
        return super().__gt__(other) \
               and self.name > other.name

    def __le__(self, other):
        return super().__le__(other) \
               and self.name <= other.name

    def __ge__(self, other):
        return super().__ge__(other) \
               and self.name >= other.name

    def __ne__(self, other):
        return super().__ne__(other) \
               and self.name != other.name


class Car(Wheeled):

    def __init__(self, kind: str, name: str, brand: str):
        super().__init__(kind, name)
        self.brand: str = brand

    def __eq__(self, other):
        return super().__eq__(other) \
               and self.brand == other.brand

    def __lt__(self, other):
        return super().__lt__(other) \
               and self.brand < other.brand

    def __gt__(self, other):
        return super().__gt__(other) \
               and self.brand > other.brand

    def __le__(self, other):
        return super().__le__(other) \
               and self.brand <= other.brand

    def __ge__(self, other):
        return super().__eq__(other) \
               and self.brand >= other.brand

    def __ne__(self, other):
        return super().__ne__(other) \
               and self.brand != other.brand


class Cycle(Wheeled):

    def __init__(self, kind: str, name: str, brand: str):
        super().__init__(kind, name)
        self.brand: str = brand

    def __eq__(self, other):
        return super().__eq__(other) \
               and self.brand == other.brand \


    def __lt__(self, other):
        return super().__lt__(other) \
               and self.brand < other.brand

    def __gt__(self, other):
        return super().__gt__(other) \
               and self.brand > other.brand

    def __le__(self, other):
        return super().__le__(other) \
               and self.brand <= other.brand

    def __ge__(self, other):
        return super().__ge__(other) \
               and self.brand >= other.brand

    def __ne__(self, other):
        return super().__ne__(other) \
               and self.brand != other.brand


class Bicycle(Cycle):

    def __init__(self, kind: str, name: str, brand: str,
                 wheel_num: int):
        super().__init__(kind, name, brand)
        self.wheel_num: int = wheel_num

    def __eq__(self, other):
        return super().__eq__(other) \
               and self.wheel_num == other.wheel_num

    def __lt__(self, other):
        return super().__lt__(other) \
               and self.wheel_num < other.wheel_num

    def __gt__(self, other):
        return super().__gt__(other) \
               and self.wheel_num > other.wheel_num

    def __le__(self, other):
        return super().__le__(other) \
               and self.wheel_num <= other.wheel_num

    def __ge__(self, other):
        return super().__ge__(other) \
               and self.wheel_num >= other.wheel_num

    def __ne__(self, other):
        return super().__ne__(other) \
               and self.wheel_num != other.wheel_num


veh1 = Vehicle("air")
veh2 = Vehicle("rail")
print("veh1 = veh2:", veh1 == veh2)
print("veh1 != veh2:", veh1 != veh2)

whe1 = Wheeled("wheeled", "car")
whe2 = Wheeled("wheeled", "car")
print("whe1 = whe2:", whe1 == whe2)
print("whe1 != whe2:", whe1 != whe2)

cr1 = Car("wheeled", "car", "Audi")
cr2 = Car("wheeled", "car", "Skoda")
print("cr1 = cr2:", cr1 == cr2)
print("cr1 <= cr2:", cr1 <= cr2)

bic1 = Bicycle("wheeled", "bicycle", "Canyon", 2)
bic2 = Bicycle("wheeled", "bicycle", "Canyon", 2)
print("bic1 = bic2:", bic1 == bic2)
print("bic1 != bic2:", bic1 != bic2)
